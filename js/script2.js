$("document").ready(function () {
	//ID
	$("#myId").append('<h1>Added by ID</h1>');
	$("#myId").css('border', '1px dotted blue');

	//Class
	$(".myClass").append('<h1>Added by Class</h1>');
	$(".myClass").css("opacity", "0.5");

	// Table
	$('#myDiv').append('<table class="table" id="tb1"><thead><tr><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th></tr></thead></table>');
	$("#tb1").addClass('table table-striped');
	$("#tb1 th").addClass('scope="row"');
	$("#tb1 th:odd").addClass("bg-secondary");
	$("#tb1 th:even").addClass("bg-info");
	
	//Class Jumbotron
	$(".jumbotron").addClass("bg-light");

	//Text
	$("#myText p:first").css("fontStyle",  "italic");
	$("#myText p:last").css("textAlign", "center");
	$("#myText p").addClass("col-lg-3 col-md-3 col-sm-6 col-xs-12");

	//Contains text
	$("p:contains('augue')").css("texthecoration",  "underline");


	$("#myDiv1 div").addClass("col-lg-4 col-md-4 col-sm-8 col-xs-12");

	$("#show").on("click", function(){
		$("#textBox1").show();
	});

	$("#hide").on("click", function(){
		$("#textBox1").hide();
	});

	$("#toggle").on("click", function(){
		$("#textBox1").toggle();
	});

	//#------------------------------------------------------------#
	//Grow
	$("#grow").on("click", function(){
		$("#textBox2").animate({width:"500px"}, 2000);
	});

	//Move
	$("#move").on("click", function(){
		$("#textBox2").animate({marginLeft:"200px"}, 1000);
	});

	//Bigger
	$("#bigger").on("click", function(){
		$("#textBox2").animate({fontSize:"40px"}, 3000);
	});

	//Many things
	$("#many").on("click", function(){
		$("#textBox2").animate({fontSize:"40px", marginLeft: "400px", width:"600px"}, 3000);
	});

	$("#textBox1").addClass("border border-dark");	
	$("#textBox2").addClass("border border-dark");	

	$("#myDiv2 div").addClass("col-lg-3 col-md-3 col-sm-6 col-xs-12");

	//#------------------------------------------------------------#
	//on click
	$("#btn1").on("click", whenMouseIsClicked);

	//mouse leave
	$("#btn1").on("mouseleave", whenMouseLeaves);


	function whenMouseIsClicked (){
		$("#btn1").html('You clicked me!');
	};

	function whenMouseLeaves (){
		$("#btn1").html('Where you are going?');
	};

	$("#textBox1").addClass("shadow p-3 mb-5 bg-white rounded");
	$("#textBox2").addClass("shadow-lg p-3 mb-5 bg-white rounded");

	//#------------------------------------------------------------#
	$("#textBox3").hide();

	//Fide in means aparecer
	$("#fade-in").on("click", function(){
		$("#textBox3").fadeIn(1000, function(){
			alert("Fade In done!");
		});
	});

	//Fide out means desaparecer
	$("#fade-out").on("click", function(){
		$("#textBox3").fadeOut(300, function(){
			alert("Fade Out done!");
		});
	});

	$("#myDiv3 a").addClass("col-lg-2 col-md-2 col-sm-4 col-xs-4");

	$("table td").each(function (){
		console.log($(this).html())
	})

	$('h6:first').next().addClass("text-uppercase");

	$( "div:has(p)" ).addClass( "test" ); //adiciona na div a classe

	//Description: Select all elements that have no children (including text nodes).
	$( "li:empty" ).text( "Was empty!" ).css( "color", "blue" );

	//Description: Select all elements that have at least one child node (either an element or text).
	$( "ul.myList:parent" ).css( "backgroundColor", "rgb(255,220,200)" );

	//Description: Get the children of each element in the set of matched elements, optionally filtered by a selector.
	$("#textBox2").children().addClass("bg-dark");


	//Description: Selects all direct child elements specified by "child" of elements specified by "parent".
	$( "ul.topnav > li" ).css( "border", "3px double red" );

	//Description: Selects all elements that are descendants of a given ancestor.
	//pega todos os input filhos de .form2
	$( ".form2 input" ).addClass("border-success");
	//pega todos os input filhos de fildset e netos de .form2
	$( ".form2 fieldset input").addClass("bg-primary");

	//Description: Selects all next elements matching "next" that are immediately preceded by a sibling "prev".
	//pega todos os input que tem um irmao label
	$( ".form3 label + input" ).val( "Labeled!" ).addClass("text-danger");


	//Description: Selects all sibling elements that follow after the "prev" element, have the same parent, and match the filtering "siblings" selector.
	//pega todas as div irmas do id #prev que sao div e estao depoiis de #prev
	$( "#prev ~ div" ).addClass( "bg-secondary" );

	$(".oi1 .oi2 div").addClass("text-warning")



});