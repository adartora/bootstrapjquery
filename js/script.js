//When the document is loaded, do something here..
$("document").ready(function() {

	//#------------------------------------------------------------#
	//jQuery to add HTML
	//.append(HTML) always in the end
	$("body").append("<h1>Append Text!</h1>");

	//#------------------------------------------------------------#
	//jQuery to add CSS
	//.css(property and value)
	$("h1").css("border", "1px solid green");

	//Set first H1 element
	$("h1:first").css("border", "2px dotted red");
	//dotted means "pontilhado"
	//solid means "sólido"

	//Set last H1 element
	$("h1:last").css("border", "2px dotted blue");

	//#------------------------------------------------------------#
	//paragraph text (<p></p>)
	var newText = $("<p>");

	//append in this case will insert inside p tag
	newText.append("<h1>I just replaced something!</h1>");
	//Set by ID and replaced content
	$("#textBox").html(newText);

	//#------------------------------------------------------------#
	//on click
	$("#textBox2").on("click", whenMouseIsClicked);

	//mouse leave
	$("#textBox2").on("mouseleave", whenMouseLeaves);

	function whenMouseIsClicked (){
		$("#textBox2").html("<p>You clicked me!</p>");
	};

	function whenMouseLeaves (){
		$("#textBox2").html("<p>Where you are going?</p>");
	};

	//#------------------------------------------------------------#
	$("#show").on("click", function(){
		$("#textBox3").show();
	});

	$("#hide").on("click", function(){
		$("#textBox3").hide();
	});

	//#------------------------------------------------------------#
	$(".textCopy").hover(highlightCopy);

	function highlightCopy(){
		$(this).toggleClass("highlight");
	};

	//#------------------------------------------------------------#
	//Fide in means aparecer
	$("#fade-in").on("click", function(){

		$("#textBox5").fadeIn(1000, function(){
			alert("Fade In done!");
		});

	});

	//Fide out means desaparecer
	$("#fade-out").on("click", function(){

		$("#textBox5").fadeOut(300, function(){
			alert("Fade Out done!");
		});

	});

	//#------------------------------------------------------------#
	//Slide up
	$("#slide-up").on("click", function(){
		$("#textBox6").slideUp(2000, "linear").delay(1000);
	});

	//Slide Down
	$("#slide-down").on("click", function(){
		$("#textBox6").slideDown(2000, "linear");
	});

	//Slide Toggle
	$("#toggleSlide").on("click", function(){
		$("#textBox6").slideToggle("fast", function(){
			alert("Done");
		});
	});

	//#------------------------------------------------------------#
	//Toggle means alternância (efeito)
	$("#toggle").on("click", function(){
		$("#textBox7").toggle(4000, "swing");
	});

	//#------------------------------------------------------------#
	//Grow
	$("#grow").on("click", function(){
		$("#textBox7").animate({width:"500px"}, 2000);
	});

	//Move
	$("#move").on("click", function(){
		$("#textBox7").animate({marginLeft:"200px"}, 1000);
	});

	//Bigger
	$("#bigger").on("click", function(){
		$("#textBox7").animate({fontSize:"40px"}, 3000);
	});

	//Many things
	$("#many").on("click", function(){
		$("#textBox7").animate({fontSize:"40px", marginLeft: "400px", width:"600px"}, 3000);
	});

	//#------------------------------------------------------------#
	//First element
	$("#container p:first").css("border", "2px dotted red");
	//Even elements (par)
	$("#container p:even").css("backgroundColor", "lightblue");
	//Odd elements (ímpar)
	$("#container p:odd").css("backgroundColor", "gray");
	//Last element
	$("#container p:last").css("border", "2px dotted yellow");

	//#------------------------------------------------------------#
	// Div tag
	$("div h4").css("border", "2px dotted yellow");

	// Div tag class
	$("div h4.myClass").css("backgroundColor", "orange");
	$("div h4.myClass").css("opacity", "0.5");

	// Div ul class
	$("div ul.myList").css("textDecoration",  "underline");
	$("div ul.myList").css("fontStyle",  "italic");

	//#------------------------------------------------------------#
	//Contains text
	$("h4:contains('Lorem')").css("textAlign", "center");

	//#------------------------------------------------------------#
	$("#content p:nth-child(2)").css("backgroundColor", "red");
	$("#content p:nth-child(1)").append("<p>Juice</p>");

	//#------------------------------------------------------------#
	//Append
	$(".myAppend").append("NEW TEXT HERE");

	//Prepend
	$(".myPrepend").prepend("NEW TEXT HERE");

	//#------------------------------------------------------------#
	//wrap
	$("#newTag").wrap("<a href='#'>NEW TAG</a>");

	//empty tag 
	$("#oldTag").empty();
	//$("#oldTag").replaceAll();
	//$("#oldTag").replaceWith();

	//#------------------------------------------------------------#
	$("#add-class").on("click", function(){
		$("p:first").addClass('myColor');
	});

	$("#remove-class").on("click", function(){
		$("p:first").removeClass('myColor');
	});

	$("#toggle-class").on("click", function(){
		$("p:first").toggleClass('myColor');
	});

	$("#increase-class").on("click", function(){
		$("p:first").css('font-size', "+=1px");
	});
	

});